#include <rl_tools/nn_models/multi_agent_wrapper/model.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
#include <rl_tools/nn/layers/standardize/layer.h>
#include <rl_tools/nn_models/sequential/model.h>
#include <rl_tools/nn_models/mlp_unconditional_stddev/network.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/layers/dense/layer.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/layers/dense/layer.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/layers/dense/layer.h>
// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/matrix/matrix.h>
#include <rl_tools/nn/parameters/parameters.h>
#include <rl_tools/nn_models/sequential/model.h>

// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/tensor/tensor.h>

// NOTE: This code export assumes that the endianness of the target platform is the same as the endianness of the source platform
#include <rl_tools/containers/tensor/tensor.h>

namespace rl_tools_export {
    namespace model {
        namespace content {
            namespace layer_0 {
                namespace mean {
                    namespace parameters_memory {
                        static_assert(sizeof(unsigned char) == 1);
                        alignas(double) const unsigned char memory[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                        using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 1, 12, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                        const CONTAINER_TYPE container = {(double*)memory}; 
                    }
                    using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Normal, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Constant>;
                    const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                }
                namespace precision {
                    namespace parameters_memory {
                        static_assert(sizeof(unsigned char) == 1);
                        alignas(double) const unsigned char memory[] = {0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 240, 63};
                        using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 1, 12, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                        const CONTAINER_TYPE container = {(double*)memory}; 
                    }
                    using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Normal, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Constant>;
                    const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                }
            }
            namespace layer_0 {
                using CONFIG = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::standardize::Configuration<double, unsigned long long>; 
                using TEMPLATE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::standardize::BindConfiguration<CONFIG>;
                using INPUT_SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 6, 12>;
                using CAPABILITY = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::capability::Backward<true>;
                using TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::standardize::Layer<CONFIG, CAPABILITY, INPUT_SHAPE>;
                const TYPE module = {{mean::parameters, precision::parameters}};
                template <typename T_TYPE = TYPE>
                const T_TYPE factory = {{mean::parameters, precision::parameters}};
                template <typename T_TYPE = TYPE>
                const T_TYPE factory_function(){return T_TYPE{{mean::parameters, precision::parameters}};}
            }
            namespace layer_1 {
                namespace input_layer {
                    namespace weights {
                        namespace parameters_memory {
                            static_assert(sizeof(unsigned char) == 1);
                            alignas(double) const unsigned char memory[] = {224, 140, 238, 181, 98, 247, 207, 63, 219, 124, 244, 176, 94, 123, 203, 191, 26, 190, 252, 204, 147, 112, 210, 63, 131, 189, 242, 167, 212, 128, 195, 191, 116, 66, 40, 119, 50, 146, 174, 191, 238, 218, 42, 148, 37, 145, 176, 191, 192, 134, 135, 49, 184, 22, 185, 63, 36, 215, 193, 208, 229, 23, 208, 63, 254, 83, 238, 242, 184, 151, 201, 63, 124, 117, 136, 103, 54, 153, 187, 191, 160, 175, 171, 226, 172, 6, 141, 63, 200, 146, 201, 225, 43, 183, 160, 191, 64, 150, 28, 223, 129, 88, 148, 63, 32, 18, 59, 106, 150, 151, 206, 63, 240, 31, 136, 99, 1, 77, 153, 191, 172, 185, 131, 15, 89, 124, 164, 191, 58, 10, 224, 51, 216, 57, 208, 63, 244, 253, 176, 6, 189, 146, 196, 63, 20, 136, 126, 173, 185, 235, 191, 63, 18, 144, 15, 146, 192, 95, 198, 63, 96, 31, 33, 230, 164, 23, 206, 191, 224, 208, 49, 158, 188, 118, 133, 63, 244, 255, 33, 79, 173, 249, 202, 63, 78, 144, 204, 211, 0, 83, 200, 63, 23, 246, 14, 109, 148, 197, 192, 191, 210, 237, 208, 250, 64, 73, 208, 191, 76, 247, 181, 233, 78, 52, 185, 63, 56, 236, 181, 165, 174, 130, 171, 63, 84, 220, 107, 53, 234, 94, 185, 63, 100, 139, 138, 116, 103, 19, 170, 191, 71, 185, 210, 243, 234, 89, 198, 191, 226, 161, 64, 186, 212, 23, 191, 191, 130, 43, 246, 70, 151, 114, 202, 191, 222, 191, 67, 195, 234, 239, 196, 63, 124, 62, 111, 62, 147, 218, 169, 191, 248, 32, 183, 159, 106, 54, 209, 191, 68, 38, 70, 171, 2, 190, 183, 63, 218, 93, 155, 17, 97, 200, 189, 191, 112, 49, 27, 11, 124, 216, 159, 191, 68, 36, 81, 175, 4, 137, 196, 191, 160, 58, 113, 188, 91, 135, 207, 191, 96, 247, 206, 55, 183, 47, 146, 191, 135, 49, 96, 106, 220, 215, 205, 191, 52, 211, 199, 84, 52, 207, 205, 63, 175, 119, 207, 115, 182, 30, 204, 191, 32, 63, 28, 25, 184, 82, 141, 63, 183, 143, 14, 134, 70, 197, 206, 191, 216, 182, 233, 66, 112, 206, 206, 63, 252, 254, 94, 85, 237, 183, 189, 191, 24, 76, 212, 213, 25, 242, 168, 63, 216, 230, 141, 87, 219, 123, 163, 63, 236, 243, 15, 44, 28, 215, 176, 63, 0, 138, 42, 200, 130, 222, 208, 63, 55, 254, 112, 64, 242, 169, 193, 191, 69, 26, 14, 184, 208, 224, 195, 191, 0, 58, 139, 149, 100, 198, 147, 63, 154, 134, 134, 84, 26, 160, 208, 63, 64, 14, 94, 20, 177, 105, 112, 191, 112, 161, 93, 185, 215, 0, 152, 63, 124, 8, 205, 51, 156, 158, 195, 63, 170, 238, 10, 132, 87, 155, 202, 191, 222, 123, 136, 206, 222, 155, 197, 63, 99, 43, 92, 209, 192, 95, 209, 191, 0, 136, 171, 232, 51, 80, 204, 63, 64, 168, 137, 146, 63, 29, 152, 63, 224, 222, 60, 126, 36, 193, 158, 191, 144, 102, 127, 191, 170, 250, 204, 63, 244, 18, 4, 115, 222, 23, 178, 191, 208, 203, 26, 31, 137, 183, 150, 63, 36, 37, 51, 56, 145, 130, 182, 63, 42, 16, 104, 136, 127, 129, 180, 191, 32, 17, 210, 193, 136, 253, 164, 63, 16, 42, 215, 184, 154, 157, 203, 191, 64, 89, 44, 117, 208, 28, 188, 63, 68, 35, 164, 110, 109, 214, 181, 63, 188, 208, 69, 46, 138, 150, 181, 191, 136, 246, 33, 39, 1, 116, 195, 63, 30, 43, 189, 40, 113, 52, 181, 191, 222, 114, 146, 182, 103, 174, 194, 63, 40, 3, 198, 110, 117, 46, 204, 63, 184, 248, 153, 90, 69, 11, 210, 191, 0, 100, 191, 74, 11, 227, 81, 191, 13, 16, 190, 166, 67, 127, 207, 191, 42, 44, 56, 92, 185, 52, 197, 63};
                            using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 7, 12, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                            using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                            const CONTAINER_TYPE container = {(double*)memory}; 
                        }
                        using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Input, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Weights>;
                        const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                    }
                    namespace biases {
                        namespace parameters_memory {
                            static_assert(sizeof(unsigned char) == 1);
                            alignas(double) const unsigned char memory[] = {25, 176, 111, 13, 6, 95, 210, 63, 222, 148, 100, 42, 6, 252, 195, 191, 124, 38, 45, 130, 163, 91, 200, 63, 192, 16, 20, 95, 243, 84, 178, 63, 58, 102, 106, 67, 29, 85, 206, 63, 232, 2, 244, 218, 182, 204, 208, 191, 116, 226, 214, 164, 89, 95, 180, 63};
                            using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 1, 7, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                            using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                            const CONTAINER_TYPE container = {(double*)memory}; 
                        }
                        using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Input, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Biases>;
                        const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                    }
                }
                namespace input_layer {
                    namespace pre_activations {
                        static_assert(sizeof(unsigned char) == 1);
                        alignas(double) const unsigned char memory[] = {105, 111, 110, 60, 100, 111, 117, 98, 108, 101, 44, 32, 117, 110, 115, 105, 103, 110, 101, 100, 32, 108, 111, 110, 103, 32, 108, 111, 110, 103, 44, 32, 83, 72, 65, 80, 69, 44, 32, 116, 114, 117, 101, 44, 32, 82, 76, 95, 84, 79, 79, 76, 83, 95, 78, 65, 77, 69, 83, 80, 65, 67, 69, 95, 87, 82, 65, 80, 80, 69, 82, 32, 58, 58, 114, 108, 95, 116, 111, 111, 108, 115, 58, 58, 116, 101, 110, 115, 111, 114, 58, 58, 82, 111, 119, 77, 97, 106, 111, 114, 83, 116, 114, 105, 100, 101, 60, 83, 72, 65, 80, 69, 62, 44, 32, 116, 114, 117, 101, 62, 59, 10, 32, 32, 32, 32, 32, 32, 32, 32, 117, 115, 105, 110, 103, 32, 67, 79, 78, 84, 65, 73, 78, 69, 82, 95, 84, 89, 80, 69, 32, 61, 32, 82, 76, 95, 84, 79, 79, 76, 83, 95, 78, 65, 77, 69, 83, 80, 65, 67, 69, 95, 87, 82, 65, 80, 80, 69, 82, 32, 58, 58, 114, 108, 95, 116, 111, 111, 108, 115, 58, 58, 84, 101, 110, 115, 111, 114, 60, 83, 80, 69, 67, 62, 59, 10, 32, 32, 32, 32, 32, 32, 32, 32, 99, 111, 110, 115, 116, 32, 67, 79, 78, 84, 65, 73, 78, 69, 82, 95, 84, 89, 80, 69, 32, 99, 111, 110, 116, 97, 105, 110, 101, 114, 32, 61, 32, 123, 40, 100, 111, 117, 98, 108, 101, 42, 41, 109, 101, 109, 111, 114, 121, 125, 59, 32, 10, 32, 32, 32, 32, 125, 10, 0, 187, 124, 254, 1, 0, 0, 160, 49, 187, 124, 254, 1, 0, 0, 47, 47, 32, 78, 79, 84, 69, 58, 108, 0, 0, 108, 125, 170, 0, 0, 192, 88, 187, 124, 254, 1, 0, 0, 0, 51, 187, 124, 254, 1, 0, 0, 76, 0, 65, 0, 84, 0, 69, 0, 61, 0, 67, 0, 59, 0, 76, 0};
                        using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 6, 7, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                        const CONTAINER_TYPE container = {(double*)memory}; 
                    }
                }
                namespace input_layer {
                    using CONFIG = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::Configuration<double, unsigned long long, 7, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::activation_functions::ActivationFunction::RELU, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::DefaultInitializer<double, unsigned long long>, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Input>; 
                    using TEMPLATE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::BindConfiguration<CONFIG>;
                    using INPUT_SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 6, 12>;
                    using CAPABILITY = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::capability::Backward<true>;
                    using TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::Layer<CONFIG, CAPABILITY, INPUT_SHAPE>;
                    const TYPE module = {{weights::parameters, biases::parameters}, pre_activations::container};
                    template <typename T_TYPE = TYPE>
                    const T_TYPE factory = {{weights::parameters, biases::parameters}, pre_activations::container};
                    template <typename T_TYPE = TYPE>
                    const T_TYPE factory_function(){return T_TYPE{{weights::parameters, biases::parameters}, pre_activations::container};}
                }
                namespace hidden_layer_0 {
                    namespace weights {
                        namespace parameters_memory {
                            static_assert(sizeof(unsigned char) == 1);
                            alignas(double) const unsigned char memory[] = {204, 206, 29, 107, 1, 0, 188, 191, 82, 27, 80, 95, 54, 96, 213, 63, 228, 107, 216, 187, 156, 66, 183, 191, 12, 36, 219, 133, 154, 112, 201, 63, 52, 248, 229, 231, 123, 71, 202, 63, 140, 52, 35, 32, 91, 56, 195, 191, 88, 165, 117, 42, 211, 99, 202, 63, 32, 2, 87, 81, 35, 111, 174, 63, 136, 126, 29, 48, 1, 193, 215, 191, 152, 217, 95, 176, 10, 58, 196, 63, 80, 90, 249, 60, 226, 185, 150, 191, 40, 184, 18, 48, 8, 154, 201, 63, 208, 237, 211, 212, 137, 61, 153, 191, 168, 172, 174, 154, 232, 89, 198, 191, 80, 223, 156, 21, 85, 221, 163, 63, 48, 188, 165, 206, 153, 204, 212, 191, 144, 39, 241, 73, 104, 78, 149, 191, 52, 207, 149, 72, 90, 125, 199, 63, 154, 28, 238, 138, 85, 206, 205, 191, 64, 78, 207, 138, 28, 160, 155, 191, 221, 82, 86, 31, 253, 22, 202, 191, 102, 170, 20, 50, 216, 50, 204, 191, 57, 172, 213, 16, 238, 205, 213, 191, 224, 155, 157, 69, 108, 51, 141, 63, 9, 118, 37, 187, 193, 181, 207, 191, 224, 98, 168, 156, 178, 16, 180, 191, 60, 94, 181, 231, 213, 254, 210, 191, 192, 237, 228, 81, 27, 96, 126, 63, 95, 70, 84, 180, 132, 113, 196, 191, 38, 113, 143, 250, 106, 244, 201, 191, 168, 96, 80, 246, 33, 160, 214, 63, 64, 111, 76, 177, 26, 115, 197, 191, 123, 80, 61, 151, 62, 198, 201, 191, 164, 181, 118, 111, 161, 194, 211, 191, 80, 180, 208, 105, 72, 30, 171, 63, 204, 150, 117, 170, 30, 147, 185, 191, 4, 59, 22, 22, 249, 72, 206, 63, 116, 155, 96, 28, 180, 87, 196, 191, 192, 132, 60, 205, 217, 8, 197, 63, 20, 165, 90, 247, 130, 219, 181, 63, 48, 203, 101, 63, 219, 73, 172, 191, 184, 157, 119, 194, 246, 94, 200, 63, 96, 61, 112, 115, 243, 183, 171, 191, 108, 251, 163, 176, 84, 185, 186, 191, 113, 207, 27, 140, 115, 197, 208, 191, 150, 199, 233, 207, 169, 38, 213, 63, 82, 116, 84, 101, 75, 37, 212, 63, 240, 252, 229, 162, 48, 72, 203, 63, 136, 13, 82, 59, 0, 131, 182, 63};
                            using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 7, 7, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                            using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                            const CONTAINER_TYPE container = {(double*)memory}; 
                        }
                        using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Normal, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Weights>;
                        const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                    }
                    namespace biases {
                        namespace parameters_memory {
                            static_assert(sizeof(unsigned char) == 1);
                            alignas(double) const unsigned char memory[] = {9, 116, 107, 77, 22, 23, 213, 191, 230, 103, 91, 63, 131, 202, 208, 191, 176, 30, 225, 33, 15, 12, 208, 63, 192, 71, 232, 240, 102, 137, 121, 63, 239, 36, 151, 92, 156, 63, 211, 191, 64, 23, 21, 46, 238, 195, 175, 191, 40, 40, 85, 164, 103, 240, 171, 191};
                            using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 1, 7, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                            using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                            const CONTAINER_TYPE container = {(double*)memory}; 
                        }
                        using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Normal, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Biases>;
                        const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                    }
                }
                namespace hidden_layer_0 {
                    namespace pre_activations {
                        static_assert(sizeof(unsigned char) == 1);
                        alignas(double) const unsigned char memory[] = {32, 48, 44, 32, 48, 44, 32, 48, 44, 32, 48, 44, 32, 48, 44, 32, 48, 44, 32, 48, 44, 32, 48, 44, 32, 48, 125, 59, 10, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 117, 115, 105, 110, 103, 32, 67, 79, 78, 84, 65, 73, 78, 69, 82, 95, 83, 80, 69, 67, 32, 61, 32, 82, 76, 95, 84, 79, 79, 76, 83, 95, 78, 65, 77, 69, 83, 80, 65, 67, 69, 95, 87, 82, 65, 80, 80, 69, 82, 32, 58, 58, 114, 108, 95, 116, 111, 111, 108, 115, 58, 58, 109, 97, 116, 114, 105, 120, 58, 58, 83, 112, 101, 99, 105, 102, 105, 99, 97, 116, 105, 111, 110, 60, 100, 111, 117, 98, 108, 101, 44, 32, 117, 110, 115, 105, 103, 110, 101, 100, 32, 108, 111, 110, 103, 32, 108, 111, 110, 103, 44, 32, 49, 44, 32, 49, 50, 44, 32, 116, 114, 117, 101, 44, 32, 82, 76, 95, 84, 79, 79, 76, 83, 95, 78, 65, 77, 69, 83, 80, 65, 67, 69, 95, 87, 82, 65, 80, 80, 69, 82, 32, 58, 58, 114, 108, 95, 116, 111, 111, 108, 115, 58, 58, 109, 97, 116, 114, 105, 120, 58, 58, 108, 97, 121, 111, 117, 116, 115, 58, 58, 82, 111, 119, 77, 97, 106, 111, 114, 65, 108, 105, 103, 110, 109, 101, 110, 116, 60, 117, 110, 115, 105, 103, 110, 101, 100, 32, 108, 111, 110, 103, 32, 108, 111, 110, 103, 44, 32, 49, 62, 62, 59, 10, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 117, 115, 105, 110, 103, 32, 67, 79, 78, 84, 65, 73, 78, 69, 82, 95, 84, 89, 80, 69, 32, 61, 32, 82, 76, 95, 84, 79, 79, 76, 83, 95, 78, 65, 77};
                        using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 6, 7, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                        const CONTAINER_TYPE container = {(double*)memory}; 
                    }
                }
                namespace hidden_layer_0 {
                    using CONFIG = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::Configuration<double, unsigned long long, 7, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::activation_functions::ActivationFunction::RELU, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::DefaultInitializer<double, unsigned long long>, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Normal>; 
                    using TEMPLATE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::BindConfiguration<CONFIG>;
                    using INPUT_SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 6, 7>;
                    using CAPABILITY = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::capability::Backward<true>;
                    using TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::Layer<CONFIG, CAPABILITY, INPUT_SHAPE>;
                    const TYPE module = {{weights::parameters, biases::parameters}, pre_activations::container};
                    template <typename T_TYPE = TYPE>
                    const T_TYPE factory = {{weights::parameters, biases::parameters}, pre_activations::container};
                    template <typename T_TYPE = TYPE>
                    const T_TYPE factory_function(){return T_TYPE{{weights::parameters, biases::parameters}, pre_activations::container};}
                }
                namespace output_layer {
                    namespace weights {
                        namespace parameters_memory {
                            static_assert(sizeof(unsigned char) == 1);
                            alignas(double) const unsigned char memory[] = {76, 21, 13, 54, 73, 221, 189, 63, 203, 97, 82, 233, 90, 233, 209, 191, 74, 88, 128, 253, 197, 229, 195, 191, 248, 246, 238, 156, 80, 239, 186, 191, 197, 205, 248, 66, 26, 206, 197, 191, 217, 55, 71, 27, 116, 156, 212, 191, 213, 61, 248, 119, 84, 212, 208, 191, 70, 128, 200, 119, 129, 57, 213, 63, 76, 84, 227, 81, 25, 240, 184, 191, 76, 135, 138, 197, 22, 197, 213, 191, 252, 243, 136, 91, 137, 80, 199, 63, 141, 195, 69, 39, 191, 122, 195, 191, 1, 76, 177, 199, 33, 155, 204, 191, 194, 80, 164, 144, 159, 78, 215, 63};
                            using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 2, 7, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                            using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                            const CONTAINER_TYPE container = {(double*)memory}; 
                        }
                        using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Output, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Weights>;
                        const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                    }
                    namespace biases {
                        namespace parameters_memory {
                            static_assert(sizeof(unsigned char) == 1);
                            alignas(double) const unsigned char memory[] = {217, 88, 163, 61, 182, 114, 214, 191, 82, 25, 120, 222, 230, 94, 208, 191};
                            using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 1, 2, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                            using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                            const CONTAINER_TYPE container = {(double*)memory}; 
                        }
                        using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Output, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Biases>;
                        const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                    }
                }
                namespace output_layer {
                    namespace pre_activations {
                        static_assert(sizeof(unsigned char) == 1);
                        alignas(double) const unsigned char memory[] = {160, 49, 187, 124, 254, 1, 0, 0, 80, 211, 185, 124, 254, 1, 0, 0, 58, 102, 97, 99, 116, 111, 114, 121, 60, 116, 121, 112, 101, 110, 97, 109, 101, 32, 84, 95, 84, 89, 80, 69, 58, 58, 83, 80, 69, 67, 58, 58, 73, 78, 80, 85, 84, 95, 76, 65, 89, 69, 82, 62, 44, 32, 123, 104, 105, 100, 100, 101, 110, 95, 108, 97, 121, 101, 114, 95, 48, 58, 58, 102, 97, 99, 116, 111, 114, 121, 60, 116, 121, 112, 101, 110, 97, 109, 101, 32};
                        using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 6, 2, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                        const CONTAINER_TYPE container = {(double*)memory}; 
                    }
                }
                namespace output_layer {
                    using CONFIG = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::Configuration<double, unsigned long long, 2, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::activation_functions::ActivationFunction::IDENTITY, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::DefaultInitializer<double, unsigned long long>, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Output>; 
                    using TEMPLATE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::BindConfiguration<CONFIG>;
                    using INPUT_SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 6, 7>;
                    using CAPABILITY = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::capability::Backward<true>;
                    using TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::Layer<CONFIG, CAPABILITY, INPUT_SHAPE>;
                    const TYPE module = {{weights::parameters, biases::parameters}, pre_activations::container};
                    template <typename T_TYPE = TYPE>
                    const T_TYPE factory = {{weights::parameters, biases::parameters}, pre_activations::container};
                    template <typename T_TYPE = TYPE>
                    const T_TYPE factory_function(){return T_TYPE{{weights::parameters, biases::parameters}, pre_activations::container};}
                }
                namespace log_std {
                    namespace parameters_memory {
                        static_assert(sizeof(unsigned char) == 1);
                        alignas(double) const unsigned char memory[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                        using CONTAINER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::Specification<double, unsigned long long, 1, 2, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::matrix::layouts::RowMajorAlignment<unsigned long long, 1>>;
                        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Matrix<CONTAINER_SPEC>;
                        const CONTAINER_TYPE container = {(double*)memory}; 
                    }
                    using PARAMETER_SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::spec<parameters_memory::CONTAINER_TYPE, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::groups::Output, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::categories::Weights>;
                    const RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::parameters::Plain::instance<PARAMETER_SPEC> parameters = {parameters_memory::container};
                }
                using CONFIG = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::mlp::Configuration<double, unsigned long long, 2, 3, 7, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::activation_functions::ActivationFunction::RELU, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::activation_functions::ActivationFunction::IDENTITY, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::layers::dense::DefaultInitializer<double, unsigned long long>>; 
                using TEMPLATE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::mlp_unconditional_stddev::BindConfiguration<CONFIG>;
                using INPUT_SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 6, 12>;
                using CAPABILITY = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::capability::Backward<true>;
                using TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::mlp_unconditional_stddev::NeuralNetwork<CONFIG, CAPABILITY, INPUT_SHAPE>;
                const TYPE module = {{{input_layer::factory<TYPE::SPEC::INPUT_LAYER>, {hidden_layer_0::factory<TYPE::SPEC::HIDDEN_LAYER>}, output_layer::factory<TYPE::SPEC::OUTPUT_LAYER>}, log_std::parameters}};
                template <typename T_TYPE = TYPE>
                const T_TYPE factory = {{{input_layer::factory<typename T_TYPE::SPEC::INPUT_LAYER>, {hidden_layer_0::factory<typename T_TYPE::SPEC::HIDDEN_LAYER>}, output_layer::factory<typename T_TYPE::SPEC::OUTPUT_LAYER>}, log_std::parameters}};
                template <typename T_TYPE = TYPE>
                const T_TYPE factory_function(){return T_TYPE{{{input_layer::factory<typename T_TYPE::SPEC::INPUT_LAYER>, {hidden_layer_0::factory<typename T_TYPE::SPEC::HIDDEN_LAYER>}, output_layer::factory<typename T_TYPE::SPEC::OUTPUT_LAYER>}, log_std::parameters}};}
            }
            namespace model_definition {
                using CAPABILITY = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::capability::Backward<true>; 
                template <typename T_CONTENT, typename T_NEXT_MODULE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::sequential::OutputModule>
                using Module = typename RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::sequential::Module<T_CONTENT, T_NEXT_MODULE>;
                using MODULE_CHAIN = Module<layer_0::TEMPLATE, Module<layer_1::TEMPLATE>>;
                using MODEL = typename RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::sequential::Build<CAPABILITY, MODULE_CHAIN, layer_0::INPUT_SHAPE>;
            }
            using TYPE = model_definition::MODEL;
            const TYPE module = {layer_0::factory<TYPE::CONTENT>, {layer_1::factory<TYPE::NEXT_MODULE::CONTENT>, {}}};
            template <typename T_TYPE = TYPE>
            const T_TYPE factory = {layer_0::factory<typename T_TYPE::CONTENT>, {layer_1::factory<typename T_TYPE::NEXT_MODULE::CONTENT>, {}}};
            template <typename T_TYPE = TYPE>
            const T_TYPE factory_function(){return T_TYPE{layer_0::factory_function<typename T_TYPE::CONTENT>(), {layer_1::factory_function<typename T_TYPE::NEXT_MODULE::CONTENT>(), {}}};}
        }
        using CONFIG = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::multi_agent_wrapper::Configuration<double, unsigned long long, 2, content::model_definition::MODULE_CHAIN>; 
        using CAPABILITY = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn::capability::Backward<true>; 
        using INPUT_SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 3, 24>;
        using TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::nn_models::multi_agent_wrapper::Module<CONFIG, CAPABILITY, INPUT_SHAPE>; 
        const TYPE module = {content::factory<TYPE::MODEL>};
        template <typename T_TYPE = TYPE>
        const T_TYPE factory = {content::factory<TYPE::MODEL>};
        template <typename T_TYPE = TYPE>
        const T_TYPE factory_function(){return {content::factory_function<TYPE::MODEL>()};}
    }

    namespace input {
        static_assert(sizeof(unsigned char) == 1);
        alignas(double) const unsigned char memory[] = {73, 70, 73, 33, 171, 43, 218, 63, 36, 13, 26, 220, 229, 89, 188, 63, 62, 31, 201, 245, 223, 248, 229, 63, 215, 28, 20, 121, 50, 96, 3, 192, 250, 212, 29, 146, 151, 193, 167, 63, 27, 104, 163, 172, 106, 242, 234, 63, 120, 248, 195, 23, 133, 92, 224, 191, 62, 10, 159, 13, 205, 177, 208, 191, 126, 144, 249, 171, 15, 240, 208, 63, 164, 199, 189, 254, 217, 236, 240, 63, 191, 221, 50, 85, 126, 32, 252, 63, 71, 58, 145, 134, 160, 122, 224, 63, 161, 172, 0, 23, 80, 117, 252, 63, 32, 118, 3, 77, 151, 26, 234, 63, 57, 151, 174, 163, 123, 237, 197, 191, 37, 251, 4, 209, 200, 4, 219, 63, 255, 253, 1, 112, 91, 9, 203, 191, 138, 145, 213, 129, 235, 129, 249, 191, 249, 40, 223, 175, 159, 141, 211, 191, 215, 243, 124, 145, 111, 248, 223, 191, 157, 48, 78, 30, 177, 14, 211, 191, 221, 196, 175, 71, 187, 239, 208, 63, 157, 15, 205, 240, 106, 124, 177, 191, 69, 162, 29, 248, 40, 164, 128, 63, 234, 249, 154, 1, 154, 246, 209, 63, 32, 112, 90, 241, 251, 36, 210, 191, 240, 236, 133, 29, 239, 245, 222, 63, 153, 224, 170, 32, 229, 141, 253, 191, 78, 44, 69, 120, 18, 33, 188, 191, 99, 188, 94, 65, 155, 82, 233, 191, 190, 78, 100, 250, 107, 207, 224, 63, 213, 47, 107, 84, 134, 79, 245, 63, 170, 13, 159, 131, 229, 115, 244, 191, 26, 251, 48, 234, 35, 181, 225, 191, 248, 59, 181, 21, 139, 15, 204, 63, 252, 53, 189, 130, 217, 212, 233, 63, 175, 245, 42, 171, 114, 218, 245, 191, 179, 60, 151, 210, 47, 22, 240, 191, 86, 80, 188, 85, 33, 83, 203, 63, 215, 49, 24, 168, 180, 78, 251, 191, 76, 155, 75, 199, 53, 16, 230, 191, 71, 142, 116, 67, 181, 42, 219, 191, 24, 71, 197, 252, 222, 240, 240, 191, 182, 153, 53, 82, 208, 58, 243, 191, 144, 152, 116, 254, 195, 178, 237, 63, 133, 181, 162, 85, 45, 31, 248, 63, 176, 84, 187, 110, 23, 88, 192, 63, 241, 60, 102, 53, 128, 137, 226, 191, 43, 220, 125, 242, 209, 48, 242, 63, 103, 193, 7, 212, 109, 136, 239, 63, 1, 46, 100, 84, 137, 103, 218, 191, 124, 147, 205, 63, 120, 47, 226, 191, 121, 21, 16, 110, 130, 208, 245, 191, 192, 38, 72, 174, 121, 98, 237, 63, 251, 222, 128, 7, 76, 0, 208, 63, 96, 46, 161, 5, 157, 255, 241, 191, 143, 234, 59, 205, 110, 109, 198, 191, 31, 50, 144, 107, 67, 230, 5, 192, 123, 131, 102, 95, 70, 160, 176, 63, 213, 115, 13, 95, 205, 8, 238, 63, 2, 150, 163, 251, 33, 231, 208, 63, 196, 43, 41, 14, 32, 82, 227, 191, 171, 72, 255, 76, 221, 234, 221, 63, 117, 64, 58, 67, 19, 230, 122, 63, 176, 29, 241, 215, 82, 155, 225, 63, 61, 219, 248, 134, 103, 103, 249, 191, 117, 163, 46, 34, 35, 178, 228, 191, 116, 86, 177, 149, 128, 101, 243, 191, 91, 30, 124, 225, 46, 240, 190, 191, 233, 96, 33, 82, 113, 185, 233, 63, 85, 144, 156, 41, 224, 217, 231, 191, 208, 67, 230, 235, 171, 193, 179, 63};
        using SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 3, 24>;
        using SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Specification<double, unsigned long long, SHAPE, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::RowMajorStride<SHAPE>, true>;
        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Tensor<SPEC>;
        const CONTAINER_TYPE container = {(double*)memory}; 
    }

    namespace output {
        static_assert(sizeof(unsigned char) == 1);
        alignas(double) const unsigned char memory[] = {164, 17, 22, 18, 185, 198, 215, 191, 60, 32, 75, 107, 52, 24, 211, 191, 138, 130, 101, 84, 220, 211, 217, 191, 186, 91, 219, 110, 145, 140, 212, 191, 42, 254, 192, 111, 145, 229, 224, 191, 160, 209, 110, 133, 68, 254, 203, 191, 149, 133, 138, 253, 94, 18, 216, 191, 87, 147, 135, 35, 114, 236, 211, 191, 178, 58, 180, 128, 183, 160, 218, 191, 88, 252, 139, 3, 220, 98, 214, 191, 125, 97, 104, 18, 29, 142, 215, 191, 174, 226, 216, 90, 10, 203, 210, 191};
        using SHAPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Shape<unsigned long long, 1, 3, 4>;
        using SPEC = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::Specification<double, unsigned long long, SHAPE, true, RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::tensor::RowMajorStride<SHAPE>, true>;
        using CONTAINER_TYPE = RL_TOOLS_NAMESPACE_WRAPPER ::rl_tools::Tensor<SPEC>;
        const CONTAINER_TYPE container = {(double*)memory}; 
    }
}
